'use strict';

const path = require('path');

let config = {
  server: {
    ip: '127.0.0.1',
    port: 1337,
    mongodb: 'mongodb://localhost/blog_portfolio'
  },
  user: {
    login: 'admin',
    password: '1'
  }
};

module.exports = config;