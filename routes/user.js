"use strict";

const express = require('express')
    , router = express.Router()
    , UserController = require('./../controllers/user')
    , md = require('./../middlewares/auth');

let userInstance = new UserController();

router.get('/login', md.auth, userInstance.login.bind(userInstance));

module.exports = router;