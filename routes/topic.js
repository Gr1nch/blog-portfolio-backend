"use strict";

const express = require('express')
    , router = express.Router()
    , TopicController = require('./../controllers/topic')
    , md = require('./../middlewares/auth');

let topicInstance = new TopicController();

router.get('/', topicInstance.getList.bind(topicInstance));
router.get('/:id', topicInstance.getById.bind(topicInstance));
router.post('/', md.auth, topicInstance.addNew.bind(topicInstance));
router.delete('/:id', md.auth, topicInstance.deleteById.bind(topicInstance));
router.put('/:id', md.auth, topicInstance.updateById.bind(topicInstance));

module.exports = router;