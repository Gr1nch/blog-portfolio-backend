"use strict";

const express = require('express')
    , router = express.Router()
    , PostController = require('./../controllers/post');

let postInstance = new PostController();

router.get('/get', postInstance.getList.bind(postInstance));

module.exports = router;
