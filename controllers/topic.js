"use strict";

const topicService = require('./../services/topic')
    , errors = require('./../services/errors')
    , _ = require('lodash')
    , mongoose = require('mongoose')
    , moment = require('moment');

module.exports = class TopicController {
  constructor() {

  }

  getList(req, res, next) {
    topicService.getList()
      .then(data => {
        req.dataOut = data;
        next();
      })
      .catch(rej => next(rej));
  }

  getById(req, res, next) {
    if (!req.params.id || !mongoose.Types.ObjectId.isValid(req.params.id)) {
      return next(errors.api.bad_params);
    }
    topicService.getById(mongoose.Types.ObjectId(req.params.id))
      .then(topic => {
        req.dataOut = topic;
        next();
      })
      .catch(rej => next(rej));
  }

  addNew(req, res, next) {
    if (!req.body || !_.isString(req.body.description) || !_.isString(req.body.title)) {
      return next(errors.api.bad_params);
    }
    let data = {
      description: req.body.description,
      title: req.body.title
    }
    topicService.new(data)
      .then(topic => {
        if (topic.__v) {
          topic = _.omit(topic, ['__v']);
        }
        req.dataOut = topic;
        next();
      })
      .catch(rej => next(rej));
  }

  deleteById(req, res, next) {
    if (!req.params.id || !mongoose.Types.ObjectId.isValid(req.params.id)) {
      return next(errors.api.bad_params);
    }
    topicService.deleteById(mongoose.Types.ObjectId(req.params.id))
      .then(topic => {
        req.dataOut = topic;
        next();
      })
      .catch(rej => next(rej));
  }

  updateById(req, res, next) {
    if (!req.params.id || !mongoose.Types.ObjectId.isValid(req.params.id)) {
      return next(errors.api.bad_params);
    }
    if (!req.body.title && !req.body.description) {
      return next(errors.api.bad_params);
    }
    let data = {};
    if (req.body.title) data.title = req.body.title;
    if (req.body.description) data.description = req.body.description;
    data.lastEdit = moment().toISOString();
    topicService.updateById(mongoose.Types.ObjectId(req.params.id), data)
      .then(topic => {
        req.dataOut = topic;
        next();
      })
      .catch(rej => next(rej));
  }
}