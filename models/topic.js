"use strict";

const mongoose = require('mongoose')
    , moment = require('moment')
    , Schema = mongoose.Schema;

moment.locale('ru');
let Topic = new Schema({
  __v: {
    type: Number,
    select: false
  },
  title: {
    type: String,
    require: true
  },
  description: String,
  created: {
    type: Date,
    default: moment().toISOString()
  },
  lastEdit: {
    type: Date,
    default: moment().toISOString()
  },
  posts: [{
    type: Schema.Types.ObjectId,
    ref: 'Post',
    default: []
  }]
});

module.exports = mongoose.model('Topic', Topic);