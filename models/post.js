"use strict";

const mongoose = require('mongoose')
    , moment = require('moment')
    , Schema = mongoose.Schema;

moment.locale('ru');
let Post = new Schema({
  __v: {
    type: Number,
    select: false
  },
  title: {
    type: String,
    require: true
  },
  description: String,
  body: String,
  created: {
    type: Date,
    default: moment().toISOString()
  },
  lastEdit: {
    type: Date,
    default: moment().toISOString()
  },
  tags: [{type: String, default: []}],
  topic: {type: Schema.Types.ObjectId, ref: 'Topic'}
});

module.exports = mongoose.model('Post', Post);
