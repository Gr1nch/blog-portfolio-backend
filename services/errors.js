'use strict';

module.exports = {
  api: {
    default: {
      code: 1000,
      message: 'Unknown api error'
    },
    bad_params: {
      code: 1001,
      message: 'Wrong input parameters'
    },
    not_found: {
      code: 1002,
      message: 'Route not found'
    },
    access_denied: {
      code: 1005,
      message: 'Access denied for this action'
    }
  },
  user: {
    token_expired: {
      code: 2001,
      message: 'Token is expired. Need login'
    },
    wrong_data: {
      code: 2002,
      message: 'Wrong login or password'
    },
    is_required: {
      code: 2003,
      message: ''
    },
    not_found: {
      code: 2004,
      message: 'User not found'
    },
    token_not_found: {
      code: 2005,
      message: 'Token not found'
    }
  },
  topic: {
    not_found: {
      code: 2004,
      message: 'Topic not found'
    }
  }
}
