"use strict";

const Topic = require('./../models/topic')
    , errors = require('./../services/errors')
    , _ = require('lodash')
    , moment = require('moment');

module.exports = {
  getList: function() {
    return new Promise((resolve, reject) => {
      Topic.find()
        .then(topics => {
          if (topics) {
            resolve(topics);
          } else {
            reject(errors.topic.not_found);
          }
        })
        .catch(rej => {
          reject(rej);
        });
    });
  },
  getById: function(id) {
    return new Promise((resolve, reject) => {
      Topic.findOne({_id: id})
        .then(topic => {
          if (topic) {
            resolve(topic);
          } else {
            reject(errors.topic.not_found);
          }
        })
        .catch(rej => {
          reject(rej);
        });
    })
  },
  new: function(data) {
    return new Promise((resolve, reject) => {
      let topic = new Topic(data);
      topic.save()
        .then(topicOut => {
          if (topicOut) {
            resolve(topicOut);
          } else {
            reject(errors.api.default)
          }
        })
        .catch(rej => {
          reject(rej);
        });
    });
  },
  deleteById: function(id) {
    return new Promise((resolve, reject) => {
      Topic.remove({_id: id})
        .then(res => {
          resolve(res);
        })
        .catch(rej => {
          reject(rej);
        });
    });
  },
  updateById: function(id, data) {
    return new Promise((resolve, reject) => {
      Topic.findOneAndUpdate({_id: id}, {$set: data}, {new: true})
        .then(topic => {
          if (topic) {
            resolve(topic);
          } else {
            reject(errors.topic.not_found);
          }
        })
        .catch(rej => {
          reject(rej);
        });
    });
  }
}
