'use strict';

const mongoose = require('mongoose'),
      path = require('path'),
      appConfig = require('./../config');

module.exports = new Promise((resolve, reject) => {
  try {
    mongoose.Promise = global.Promise;
    mongoose.connect(appConfig.server.mongodb);
    let db = mongoose.connection;

    db.on('error', (err) => {
      reject(err);
    });
    db.once('open', () => {
      resolve(db);
    });
  } catch (e) {
    reject(e);
  }
});