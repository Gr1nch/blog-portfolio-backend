'use strict';

const express = require('express')
    , path = require('path')
    , bodyParser = require('body-parser')
    , _ = require('lodash')
    , app = express()
    , http = require('http').Server(app)
    , appConfig = require('./config')

    , mongo = require('./services/mongo')
    , errors = require('./services/errors');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, token');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  next();
});

app.get('/', (req, res) => {
  res.send('test api recipe book');
});
app.use('/api/user', require('./routes/user'));
app.use('/api/topic', require('./routes/topic'));
app.use('/api/post', require('./routes/post'));

app.use((req, res, next) => {
  if (!req.dataOut) {
    return next();
  }
  if (req.dataOut._id) { //TODO: Need fix '_id' to 'id'.
    req.dataOut['id'] = req.dataOut['_id'];
    req.dataOut = _.omit(req.dataOut, '_id');
  }
  res.send({
    success: true,
    data: req.dataOut
  })
})

app.use((req, res, next) => {
  next(errors.api.not_found);
});

app.use((err, req, res, next) => {
  // if (!Object.keys(err).length) {
  //   err = errors.api.default;
  // }
  res.send({
    success: false,
    error: err
  });
});

mongo
  .then(() => {
    console.info('Mongo database connected');
    http.listen(appConfig.server.port, appConfig.server.ip, () => {
      console.log(`listening on ${appConfig.server.port} port`);
    });
  })
  .catch(e => {
    console.error(e);
    process.exit(0);
  });
