"use strict";

const appConfig = require('./../config')
    , basicAuth = require('basic-auth')
    , errors = require('./../services/errors');

module.exports.auth = function(req, res, next) {
  let data = basicAuth(req);
  if (data && data.name === appConfig.user.login && data.pass === appConfig.user.password) {
    next();
  } else {
    next(errors.user.not_found);
  }
};